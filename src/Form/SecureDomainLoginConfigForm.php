<?php

namespace Drupal\secure_domain_login\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Includes SecureDomainLogin Configuration Form.
 */
class SecureDomainLoginConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'secure_domain_login.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'secure_domain_login_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('secure_domain_login.config');

    $form['whitelist_domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('WhiteList Domains'),
      '#default_value' => $config->get('whitelist_domains'),
      '#description' => $this->t("Add multiple domains as comma(,) separated without space."),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('secure_domain_login.config')
      ->set('whitelist_domains', $form_state->getValue('whitelist_domains'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
