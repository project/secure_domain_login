<?php

namespace Drupal\secure_domain_login\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event Subscriber EventSubscriber.
 */
class SecureDomainLogin implements EventSubscriberInterface {

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The admin toolbar tools configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * To get the current request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs an EventSubscriber.
   *
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current route match.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   To get the current request.
   */
  public function __construct(CurrentPathStack $current_path, ConfigFactoryInterface $config_factory, RouteMatchInterface $route_match, RequestStack $requestStack) {
    $this->currentPath = $current_path;
    $this->config = $config_factory;
    $this->currentRouteMatch = $route_match;
    $this->requestStack = $requestStack;
  }

  /**
   * Code that should be triggered on event specified.
   */
  public function onRespond(ResponseEvent $event) {
    $current_path = $this->currentPath->getPath();
    $request = $this->requestStack->getCurrentRequest();
    $current_host = $request->getHttpHost();

    // List of all the domain which are allowed for accessing the website
    // backend.
    $whitelist_domains = explode(',', $this->config->get('secure_domain_login.config')->get('whitelist_domains'));

    // Check to verify if the current domain is not part of allowed domain.
    if (!in_array($current_host, $whitelist_domains)) {
      if (str_contains($current_path, '/user')) {
        $redirect_url = Url::fromUri('internal:' . $this->config->get('system.site')->get('page.front'));
        $event->setResponse(new RedirectResponse($redirect_url->toString()));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = 'onRespond';
    return $events;
  }

}
